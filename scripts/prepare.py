import argparse
import pandas as pd
from os.path import join, realpath, dirname, pardir, exists, isfile, isdir, normpath
import numpy as np
import scipy.stats
import os
import logging
import itertools
import json
import random

def simplify(df):
    """
        Flatten the IsTotal and Import columns and merge KPI, Resources and Technology into KPI*
    """
    # Flatten the IsTotal and Import columns
    imports = set(df["Import"].unique())
    assert imports <= {"Imports", "Exports", None}
    assert set(df[~df["Import"].isnull()]["KPI"].unique()) == {"exports_and_imports"}
    for e in imports:
        if e is None:
            continue
        df_e = df["Import"] == e
        df.loc[df_e, "KPI"] = e.lower()

    is_total = set(df["IsTotal"].unique())
    assert is_total <= {"Power demand", "Total of selected technologies", None}
    assert set(df[~df["IsTotal"].isnull()]["KPI"].unique()) == {"demand_peak"}
    for e in imports:
        if e is None:
            continue
        df_e = df["IsTotal"] == e
        df.loc[df_e, "KPI"] = "_".join(e.lower().split(" "))

    # Merge KPI, Resources and Technology into KPI* (Technology is None for loss of load)
    df["KPI*"] = df["KPI"] + "_" + df["Resources"] + "_" + \
                 df["Technology"].astype(str).str.replace("None","").str.replace(" ", "_").str.lower()

    df.drop(columns=["KPI", "Import", "IsTotal", "Resources", 'AssetName', 'Technology', 'MC', 'TC'], inplace=True)
    return df

def merge_assets(df, index_cols):
    """
        Merge Assets by Country
    """
    # Count how many values we will aggregate by merging assets
    count = df.groupby(index_cols)["Value"].count()
    logging.info(f"Merging values (sum) for {len(count.index)} entries made of {index_cols}")
    max_agg = count[count == count.max()].reset_index()
    logging.info(f"On average, we merge {count.mean():0.1f} values, and at most {count.max()} "
          f"(for {max_agg['Country'].unique()} with {max_agg['KPI*'].unique()})")
    count = count.reset_index()
    logging.info(f"No aggregation for {count[count['Value'] == 1]['KPI*'].nunique()/count['KPI*'].nunique():1.0%} KPI*s")

    # TODO: Custom aggregation function per KPI instead of sum for everything
    return df.groupby(index_cols)["Value"].sum().reset_index()


def boxplot_statistics(x):
    """
        Compute statistics for an array: average, median, 95-percentile, min, max
    """
    avg = x.mean()
    med = x.median()
    perc95 = np.percentile(x, 95)
    return str(dict(avg=avg, med=med, perc95=perc95, min=x.min(), max=x.max()))


def load_input_data(input_file):
    df = pd.read_parquet(input_file)
    df["Scenario"] = df["TC"].astype(str) + "_" + df["MC"].astype(str)
    df.drop(columns=["simulation"], inplace=True)
    df = merge_assets(simplify(df.copy()), ["Scenario", "Country", "KPI*"])
    kpis = df["KPI*"].unique()
    countries = df["Country"].unique()
    df = pd.pivot_table(df,
                        values='Value',
                        index=['Scenario', 'Country'],
                        columns=['KPI*'],
                        aggfunc=np.nansum,
                        dropna=True).reset_index()

    df.fillna(0, inplace=True)
    return df


def precompute_files(res, output_folder):
    if not exists(output_folder):
        os.makedirs(output_folder)

    # Detail-level files
    plans = res["Plan"].unique()
    countries = res["Country"].unique()
    for plan in plans:
        for country in countries:
            cols = [c for c in res.columns if c != "Country" and c != "Plan"]
            output_file = join(output_folder, f"{plan}{country}.csv")
            res[(res["Country"] == country) & (res["Plan"] == plan)][cols]\
                .to_csv(output_file, index=False)

    # Top-level file
    cols = list(set(res.columns) - {"Scenario", "Plan", "Country"})
    res_agg = res.groupby(["Plan", "Country"])[cols]\
        .agg(boxplot_statistics)\
        .reset_index()
    res_agg.to_csv(join(output_folder, "overview.csv"), index=False)

    # Country-level files
    for country in countries:
        cols = [c for c in res_agg.columns if c != "Country"]
        output_file = join(output_folder, f"{country}.csv")
        res_agg[res_agg["Country"] == country][cols]\
            .to_csv(output_file, index=False)


def prepare_input_data(df, output_folder):
    df["Plan"] = "PlanA" # Add a single plan
    precompute_files(df, output_folder)
    return df

def prepare_dummy_data(output_folder, distribution_func, scenarios, countries, kpis, plans):
    """
        Create dummy data with given scenarios, countries, kpis and plans, using distribution_func
        with random parameters
        Expect `distribution_func` to take loc and scale as parameter
    """
    # Create rows: Scenario x Country
    df = pd.DataFrame(list(itertools.product(scenarios,countries)), columns=["Scenario", "Country"])
    n = len(df.index)
    dfs = []
    # Generate random values for each plan
    for plan in plans:
        df_ = df.copy()
        for kpi in kpis:
            loc = np.random.uniform(low=10, high=3000)
            scale = np.random.uniform(low=50, high=1000)
            df_[kpi] = distribution_func(loc=loc, scale=scale, size=n)
        df_["Plan"] = plan
        dfs.append(df_)
    res = pd.concat(dfs)
    precompute_files(res, output_folder)
    return res

def prepare_config_file(df, template_config_file, output_config_file):
    """
        Copy the template config file to the dataset folder, filling it with placeholder values
    """
    example_tags = ["TODO", "important", "discard"]
    columns = [ col for col in df.columns if col not in ["Scenario", "Plan", "Country"] ]
    plans = df["Plan"].unique()
    with open(template_config_file, 'r') as f:
        config = json.loads(f.read())
    config["selectedColumns"] = columns[:5]
    config["columnBest"] = {col: "minimum" if random.randint(0, 1) == 0 else "maximum" for col in columns}
    for plan in plans:
        config["metaColumnTags"][plan] = random.sample(example_tags, random.randint(1, 3))
    with open(output_config_file, 'w') as f:
        json.dump(config, f, indent=4)
    logging.info(f"Generated config file {config}")
    logging.info(f"Edit config file at {output_config_file}")


if __name__ == '__main__':
    script_folder = dirname(realpath("__file__"))
    logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("--output_folder", "-o", help="path to output folder to create or overwrite")
    parser.add_argument("--input_file", "-i", help="path to input gzip file") # TODO: Detect unexpected format
    parser.add_argument("--template_config_file", "-t", help="path to config file template")
    args = parser.parse_args()

    output_folder = args.output_folder
    input_file = args.input_file
    template_config_file = args.template_config_file
    output_config_file = join(output_folder, "config.json")

    if not isfile(template_config_file):
        logging.error("Could not access template file, specify path with option -t")
        exit()
    output_folder_parent = normpath(join(output_folder, pardir))
    if not isdir(output_folder_parent):
        logging.error(f"Cannot access {output_folder_parent}")
        exit()
    if not isdir(output_folder):
        logging.info(f"Output folder ({output_folder}) exists. Content will be overwritten.")
    if input_file and not isfile(input_file):
        logging.error("Could not access input file, specify path with option -i")
        exit()

    if input_file:
        df = load_input_data(input_file)
        df = prepare_input_data(df, output_folder)
        prepare_config_file(df, template_config_file, output_config_file)
    else:
        countries = ['AL', 'AT', 'BA', 'BE', 'BG', 'CH', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR',
                    'GB', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'ME', 'MK', 'MT', 'NI', 'NL',
                    'NO', 'PL', 'PT', 'RO', 'RS', 'SE', 'SI', 'SK']
        plans = ["PlanA", "PlanB", "PlanC", "PlanD"]
        kpis = [f"KPI{i+1}" for i in range(25)]
        scenarios = [f"{year}_{i}" for year in range(1990, 2021) for i in range(1, 22)]
        df = prepare_dummy_data(output_folder, scipy.stats.norm.rvs, scenarios, countries, kpis, plans)
        prepare_config_file(df, template_config_file, output_config_file)
