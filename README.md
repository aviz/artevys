# ArteVys

Analysis scripts and visual tool (cribble) for FF4EuroHPC WP4.

* [Demo](https://aviz.fr/~gricher/ff4hpc/) ([JSDoc](https://aviz.fr/~gricher/ff4hpc/docs))
* [Wiki](../../wikis): Development guide

## Content

- `scripts` contains Jupyter notebooks and Python scripts to prepare data;
- `cribble` contains the visualization prototype with dummy data.

## Dependencies

- Python 3.7 with `pip` to install dependencies and run data preparation scripts and Jupyter notebooks;
- [`npm`](https://nodejs.org/en/download/) to install dependencies and build the visualization tool.

### Using miniconda/anaconda (recommended)

1. Install the latest version of `miniconda` (if not done yet)
2. Create and activate a `conda` environment:

```
conda env create -f environment.yml
conda activate cribble
```

This will make the command `npm`, `pip`, `python` available.

### Alternative

Find options to install `npm` [here](https://github.com/nodesource/distributions).

## Cribble: Visualization tool

The tool is client-only and web-based. By default, it runs with dummy dataset pushed on the repository (placed
in `cribble/public/data/dataset1`). To prepare new data, read the [data preparation section](#Preparation-script).

### Setup

In the `cribble` folder, install dependencies:

```
npm install
npm run postinstall
```

Compiles and hot-reloads for development (serves an interface at `http://localhost:8080`):

```
npm run serve
```

To build a production version, update the `publicPath` in `vue.config.js` if necessary. For instance if the server
serves the app at `www.mydomain.com/app/cribble/`:

```
publicPath: process.env.NODE_ENV === 'production' ? '/app/cribble/' : '/',
```

And run to prepare a bundle in the `dist` folder:

```
npm run build
```

Refer to the [Vue.js deployment guide](https://cli.vuejs.org/guide/deployment.html) for more information.

### Documentation

Prepare documentation as a website with:

```
npm run generate-docs
```

HTML files are generated in the `docs` directory.

## Notebook & scripts

The `scripts` folder contains a Python notebook to explore projection parameters for scenarios (test cases) and
preparation script for `cribble`.

## Setup

To install the dependencies with pip:

```
pip install requirements.txt
```

### Jupyter notebooks

Launch a Jupyter notebook with:

```
jupyter-notebook projection.ipynb
```

You might need to activate Jupyter extensions:

```
jupyter nbextension enable --py widgetsnbextension
```

Usage:

- Edit the file path to the data file (not included in the repository)
- Make sure all cell runs correctly
- Play with the parameter widgets of the last cell.

### Preparation script

Datasets for Cribble are placed in the `cribble/public/data` folder. `dataset1` made from dummy data exists on the
repository to make the tool usable from the start.

To regenerate a dummy dataset, called `dataset2`:

```
 python scripts/prepare.py -o cribble/public/data/dataset2 -t config.template.json 
```

To prepare a dataset from an input file `kpis_all_2022.gzip`:

```
 python scripts/prepare.py -i kpis_all_2022.gzip -o cribble/public/data/dataset2 -t config.template.json
```

## Authors and acknowledgment

- Gaëlle Richer
- Jean-Daniel Fekete

## License

TODO

## Project status

Ongoing development for prototype April, 15.
