import DatasetOverview from './components/DatasetOverview'
import CountryOverview from './components/CountryOverview'
import ScenarioOverview from './components/ScenarioOverview'

export default [
    {
        path: '/',
        redirect: '/dataset1/' // Redirects to dataset1 by default
    },
    {
        path: '/:dataset',
        component: DatasetOverview,
    },
    {
        path: '/:dataset/:country',
        component: CountryOverview
    },
    {
        path: '/:dataset/:country/:plan',
        component: ScenarioOverview
    }
];
