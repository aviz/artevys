import Dexie from 'dexie'

const db = new Dexie('cribbleDB') // Singleton
const table = "annotations"

async function populate() {
    const ANNOTATIONS = [
        {
            dataset: "dataset1",
            metaColumn: "PlanA",
            user: "user1",
            column: "KPI1",
            country: "SK",
            scenario: "1992_1",
            level: "error",
            text: "Example annotation"
        },
    ]
    db[table].bulkAdd(ANNOTATIONS)
}

function openTable() {
    if (!('indexedDB' in window)) {
        console.error('This browser doesn\'t support IndexedDB')
    }
    try {
        // Check that table exists
        db.table(table)
        try {
            db[table].toArray() // Test table format
        } catch (error) {
            db[table].delete()
            initTable()
        }
    } catch {
        initTable()
    }
    console.assert(db.table(table))
}

function initTable() {
    db.close()
    db.version(1).stores({
        [table]: "++id, dataset, metaColumn, column, country, scenario, level", // Indexed props
    })
    db[table].mapToClass(Annotation)
    db.open()
    db.on("populate", populate)
}


class Annotation {
    static LEVELS = {
        "error": "#7b3294",
        "warn": "#c2a5cf",
        "info": "#f7f7f7",
        "decent": "#a6dba0",
        "good": "#008837"
    }

    constructor(dataset, metaColumn, column, country, scenario, level, user, text) {
        this.date = Date.now()
        if (!metaColumn && !column && !country && !scenario)
            throw "Missing at least one key out of metaColumn, column, country, and scenario"
        if (!level || !dataset)
            throw "Annotations require a level and a dataset"
        this.metaColumn = metaColumn
        this.column = column
        this.country = country
        this.scenario = scenario
        this.level = level || "error"
        this.user = user
        this.text = text || ""
    }

    static get(id) {
        return new Promise((resolve, reject) => {
            db[table].get(id).then((obj) => {
                if (obj) {
                    resolve(obj)
                } else {
                    reject(obj)
                }
            }).catch(reject)
        })
    }

    remove() {
        return db[table].delete(this.id)
    }

    log() {
        console.log(JSON.stringify(this))
    }

    save() {
        return new Promise((resolve, reject) => {
            if (this.id) {
                db[table].put(this, this.id).then(() => resolve(this)).catch(reject)
            } else {
                db[table].add(this).then(() => resolve(this)).catch(reject)
            }
        })
    }
}

class AnnotationProvider {
    static async count() {
        openTable()
        return db[table].count()
    }

    static getAll() {
        openTable()
        return db[table].toArray()
    }

    static async queryGroupByLevel(whereClause) {
        openTable()
        const levels = new Set((await db[table].orderBy("level").toArray()).map(a => a.level))
        let res = {}
        for (const level of [...levels]) {
            res[level] = await db[table].where({...whereClause, level: level}).toArray()
        }
        return res
    }
}


export {
    AnnotationProvider,
    Annotation
}
