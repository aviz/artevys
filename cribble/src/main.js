import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import routes from './routes.js'
import SuiVue from 'semantic-ui-vue'
import 'semantic-ui-css/semantic.min.css'
import './assets/table.css'

Vue.config.productionTip = false
Vue.use(VueRouter)
Vue.use(SuiVue)

const router = new VueRouter({
    routes: routes
});

new Vue({
    router,
    render: (h) => h(App)
}).$mount("#app")
