import * as _ from "lodash"
import {DatasetProvider} from "@/util"


let tagMixin = {
    data() {
        return {
            metaColumnFilter: [],
            metaColumnTags: {} // TODO: Replace by centralised storage
        }
    },
    created: function () {
    },
    computed: {
        activeTags() {
            return this.metaColumnFilter.filter(t => t.isActive).map(t => t.name)
        }
    },
    methods: {
        toggleMetaColumnTags(tag) {
            let tagObj = this.metaColumnFilter.filter(t => t.name === tag)[0]
            tagObj.isActive = !tagObj.isActive
        },
        initTags(tagSet) {
            const tags = Array.from(new Set(tagSet))
            this.metaColumnFilter = tags.map(tag => {
                return {isActive: true, name: tag}
            })
            this.metaColumnTags = this.config.metaColumnTags
        },
        addTag(metaCol, tag) {
            this.metaColumnTags[metaCol].push(tag)
            if (!this.metaColumnFilter.find(t => t.name === tag))
                this.metaColumnFilter.push({isActive: true, name: tag})
        },
        removeTag(metaCol, tag) {
            this.metaColumnTags[metaCol] = this.metaColumnTags[metaCol].filter(v => v !== tag)
        },
        shouldBeDisplayed(metaCol) {
            // Keep every metaColumn with at least one active tag or no tag
            return !(metaCol in this.metaColumnTags) || this.metaColumnTags[metaCol].length === 0 ||
                _.intersection(this.metaColumnTags[metaCol], this.activeTags).length > 0
        }
    }
}

let editColumnsMixin = {
    data() {
        return {
            visibleColumns: [],
            columnToAdd: null,
            addDropDown: {
                searchInMenu: {icon: 'search', iconPosition: 'left'},
                menuHeader: {icon: '', content: 'Header'},
            }
        }
    },
    created: function () {
        this.initVisibleColumns()
    },
    watch: {
        displayColumns() {
            this.initVisibleColumns()
        }
    },
    computed: {
        columns: function () {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        addableColumns: function () {
            if (this.columns.length === 0) return []
            const hiddenCols = this.columns.filter(value => !this.visibleColumns.includes(value))
            // TODO: Compute critical columns that are hidden and display warning icon (plus sort them on top)
            const criticalColumns = new Set()
            return hiddenCols.map((c) => {
                return {key: c, value: c, text: c, label: criticalColumns.has(c) ? {color: 'red'} : null}
            })
        },
    },
    methods: {
        initVisibleColumns() {
            if (this.$route.query.visibleColumns)
                this.visibleColumns = this.$route.query.visibleColumns.split(',')
            else
                this.visibleColumns = this.displayColumns
            this.computeDistributions()
        },
        computeDistributions() {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        hideColumn(col) {
            this.visibleColumns.splice(this.visibleColumns.indexOf(col), 1)
            const query = {...this.$route.query, visibleColumns: this.visibleColumns.toString()}
            this.$router.push({query})
        },
        addColumn(event) {
            if (event.target.getAttribute("role") === "option") {
                this.visibleColumns.push(event.target.innerHTML)
                const query = {...this.$route.query, visibleColumns: this.visibleColumns.toString()}
                this.$router.push({query})
                this.computeDistributions()

            }
        }
    }
}


let annotationMixin = {
    methods: {
        addAnnotation(metaColumn, column, country, scenario) {
            this.$root.$emit("annotate", {metaColumn, column, country, scenario})
        }
    }
}

let sortColumnsMixin = {
    data() {
        return {
            sortRowsBy: null,
            reverse: null,
        }
    },
    created() {
        // Set sorting based on query parameter
        if (this.$route.query.sortBy) {
            try {
                this.sortRowsBy = JSON.parse(this.$route.query.sortBy)
            } catch (e) {
                this.sortRowsBy = this.$route.query.sortBy
            }
        } else
            this.sortRowsBy = this.rowColumn
        if (this.$route.query.reverse)
            this.reverse = this.$route.query.reverse
        else
            this.reverse = false
    },
    computed: {
        rowColumn() {
            if (!('idColumn' in this))
                console.error("Mixed component should have prop 'idColumn'!")
            return this['idColumn']
        }
    },
    methods: {
        sortRowsByColumn(col) {
            if (this.sortRowsBy === col) {
                this.reverse = !this.reverse
                const query = {...this.$route.query, reverse: this.reverse, sortBy: col}
                this.$router.push({query})
            } else {
                this.sortRowsBy = col
                if (typeof col == 'object')
                    col = JSON.stringify(col)
                const query = {...this.$route.query, reverse: this.reverse, sortBy: col}
                this.$router.push({query})
            }
        },
    }
}

let distributionMixin = {
    data() {
        return {
            distributions: null,
        }
    },
    created() {
        this.computeDistributions()
    },
    computed: {
        columns: function () {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        countries: function () {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        metaColumns: function () {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        dataset() {
            console.error("Should have been overwritten in the component using this mixin!")
        },
        nbBins() {
            console.error("Should have been overwritten in the component using this mixin!")
        }
    },
    methods: {
        async computeDistributions() {
            const tableParameters = [this.dataset, this.countries, this.metaColumns, this.columns]
            const groupByMetaColumn = this.countries.length > 1
            const res = await DatasetProvider.computeDistributions(
                ...tableParameters, this.columnRanges, this.nbBins, groupByMetaColumn)
            this.distributions = Object.assign({}, this.distributions, res) // Ensures reactivity
        }
    }
}


export {
    sortColumnsMixin,
    editColumnsMixin,
    tagMixin,
    annotationMixin,
    distributionMixin
}
