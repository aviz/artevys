import embed from "vega-embed"
import resize from "vue-resize-directive"

let vegaMixin = {
    directives: {
        resize,
    },
    data() {
        return {
            loading: true,
            specModel: {
                $schema: "https://vega.github.io/schema/vega-lite/v5.json",
                height: 250,
                width: 200
            }
        }
    },
    computed: {
        specData: function () {
            console.error("Should have been overwritten in the component using this mixin!")
        }
    },
    watch: {
        data(v) {
            if (v) this.draw()
        },
        selected() {
            if (this.data) this.draw()
        }
    },
    methods: {
        async draw() {
            if (this.data && this.$refs.chart) {
                embed(this.$refs.chart, this.specData, {actions: false}).catch(console.error).then(() => {
                    this.loading = false
                })
            }
        }
    },
    mounted() {
        this.draw()
    },
}


export {
    vegaMixin
}
