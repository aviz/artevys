const {GitRevisionPlugin} = require('git-revision-webpack-plugin')

module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
    runtimeCompiler: true,
    chainWebpack: config => {
        config.plugin('define')
            .tap(args => {
                const gitRevisionPlugin = new GitRevisionPlugin()
                console.log(gitRevisionPlugin.lastcommitdatetime())
                args[0]['process.env']['LASTCOMMITDATETIME'] = JSON.stringify(gitRevisionPlugin.lastcommitdatetime())
                args[0]['process.env']['VUE_APP_COMMIT_HASH'] = JSON.stringify(gitRevisionPlugin.commithash())
                return args
            })
    },
    pluginOptions: {
        'style-resources-loader': {
            preProcessor: 'scss',
            patterns: []
        }
    }
}
